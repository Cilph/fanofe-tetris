'use strict';

const path = require('path');
const webpack = require('webpack');

const DashboardPlugin = require('webpack-dashboard/plugin');


function getProductionPlugins() {
	console.log('Starting production plugin mode');
	return [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production'),
			},
		}),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false,
			},
			comments: false,
			sourceMap: true,
		}),
		new webpack.LoaderOptionsPlugin({
			minimize: true,
			debug: false,
		}),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.OccurrenceOrderPlugin(true),
	];
}

module.exports = {
	context: path.resolve(path.join(__dirname, 'src')),
	entry: [
		'react-hot-loader/patch',
		'babel-polyfill',
		'./app.js',
	],
	output: {
		path: __dirname + '/dist/',
		publicPath: '/dist/',
		filename: 'bundle.js',
	},
	module: {
		rules: [
			{
				test: /\.js(x)?/,
				exclude: /node_modules/,
				use: [ 'react-hot-loader/webpack', 'babel-loader' ],
			},
			{
				test: /\.css$/,
				use: [ 'style-loader', 'css-loader' ],
			},
			{
				test: /\.scss$/,
				use: [ 'style-loader', 'css-loader', 'sass-loader' ],
			},
			{
				test: /\.(jpeg|jpg|png|gif|svg|woff|woff2|ttf|otf|eot)$/,
				use: [ 'file-loader' ],
			},
		],
	},
	plugins: ((process.env.NODE_ENV === 'production') ? getProductionPlugins() : [
		new DashboardPlugin(),
	]),
};
