# FanoFe

## Setup

1. Clone the repository to a local version
2. Create your new project on Bitbucket
3. Run the `git remote set-url origin [url]` command with the Git URL of your repository
4. `git push origin master` your new project :)
5. Run `yarn`. If you haven't installed yarn yet, run `npm install -g yarn` first
6. Run `npm run dev` to start developing, navigate to port 3000 in your browser to see the project

## Pulling in changes in a fork

1. Add FanoFE again as another remote to your git repository using `git remote add fanofe [url]`
2. `git pull fanofe master --no-ff`
3. Merge the new changes in (and fix potential conflicts)

If you hate conflicts, you might want to try and manually apply the changes as well.
Keep in mind that this means `git pull` will in the future find even more conflicts,
so make sure you never try to pull in new changes automatically again

## Possible Changes in the Future

### Updates of modules

Updates that can and probably should be done at some point in the (near) future:  

* __React Router 3.x -> 4.x__
  The new version features an entirely different API, which, according to the authors,
  should work much better. React Router 4 also plays nicer with stuff like hot reload.
* __React-Redux 4.x -> 5.x__
  Should be API compatible with 4.x, but semver thinks a version bump is necessary since
  a lot of internal rewriting has been going on. Internally a lot has changed, and
  some performance improvements have been added. 5.x also works better with Reacts
  context feature. We don't use that a lot at the moment, but it might be interesting
  in the near future. We probably want to try and update as soon as 5.x hits stable,
  which should be pretty soon now, since the first release candidates have been
  built already. For a full list of changes:
  [React-Redux v5.0.0-rc.1](https://github.com/reactjs/react-redux/releases/tag/v5.0.0-rc.1)

### Removal of compatibility packages

Some packages are currently included because they polyfill something, or add support for
stuff that is not needed anymore in the future, when cars will be able to fly, and IE
has died.

* __Babel Promise polyfill__
  This one is included to add Promise support to IE 11.

### Other stuff

* __Decorators__
  They are fancy, they are useful, and the syntax feels familiar to the Java Annotations.
  Currently a stage 2 proposal, which should indicate it's quite safe to use and only
  minor changes will be made to the spec.
  However, the TC39 committee is currently thinking about a sigil swap between private
  fields and decorators, which would mean that the old syntax (`@connect()`) will be
  replaced by a new syntax (`#connect()`). The TC39 committee has a meeting planned for
  the 29th of november until the 1st of december 2016. They might decide to move the spec
  forward one way or another. In the meantime we opted to use the old syntax, since
  there is a babel module for it, but we might have to rewrite it in every project we use
  if the committee decides to go through with the sigil swap.
