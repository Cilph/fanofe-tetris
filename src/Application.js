import React, { Component } from 'react';

// Add stylesheets for the application
import 'normalize.css/normalize.css';		// Don't let this fool you, normalize.css is the name of the Node module
import './modules/core/styling/main.scss';

export default class Application extends Component {
	render() {
		return <div>{this.props.children}</div>;
	}
}
