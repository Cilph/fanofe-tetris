import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import Application from './Application.js';
import TetrisPage from './modules/tetris/containers/TetrisPage.js';
import TodoPage from "./modules/todomvc-plain/components/TodoPage.js";

export default class Routes extends React.Component {
	render() {
		return <Router history={this.props.history}>
			<Route path="/" component={Application}>
                <Route path="/tetris" component={TetrisPage}/>
                <Route path="/todos" component={TodoPage}/>
			</Route>
		</Router>;
	}
}
