// React and ReactDOM dependencies
import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

// Redux dependencies
import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas.js';

// React Router and React Router Redux dependencies
import { browserHistory } from 'react-router';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';

// Load React-Intl
import enLocale from 'react-intl/locale-data/en';
import nlLocale from 'react-intl/locale-data/nl';
import deLocale from 'react-intl/locale-data/de';
import frLocale from 'react-intl/locale-data/fr';
import esLocale from 'react-intl/locale-data/es';
import { addLocaleData } from 'react-intl';
addLocaleData(enLocale);
addLocaleData(nlLocale);
addLocaleData(deLocale);
addLocaleData(frLocale);
addLocaleData(esLocale);

// Application level stuff
import LocaleWrapper from './LocaleWrapper.js';
import Routes from './Routes.js';
//let Routes = require('./Routes.js').default;
import reducers from './reducers.js';

// Set up the redux store, including the react router reducer
const sagaMiddleware = createSagaMiddleware();
const composer = (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose);
const store = createStore(
    combineReducers({ ...reducers }),
    composer(applyMiddleware(routerMiddleware(browserHistory), sagaMiddleware))
);

// Start running the main saga
sagaMiddleware.run(rootSaga);

// Connect react router with the redux store for history
const history = syncHistoryWithStore(browserHistory, store, {
	selectLocationState: state => state.router
});

// Render to DOM
function render() {
	ReactDOM.render(
		<AppContainer>
			<Provider store={store}>
				<LocaleWrapper>
					<Routes history={history} />
				</LocaleWrapper>
			</Provider>
		</AppContainer>,
		document.getElementById('app')
	);
}

render();
if (module.hot) {
	module.hot.accept('./Routes.js', render);
}
