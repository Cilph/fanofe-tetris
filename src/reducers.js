import locale from './modules/core/reducers/locale.js'
import router from './modules/core/reducers/router.js'
import TetrisReducer from "./modules/tetris/reducers/TetrisGameReducer.js"

const DEFAULT_TETRIS_STATE =  {
    main: TetrisReducer()
};

export default {
    locale,
    router,
    tetris: (state = DEFAULT_TETRIS_STATE, action) => ({
        ...state,
        main: action.scope == "tetris.main" ? TetrisReducer(state.main, action) : (state).main
    })
};

