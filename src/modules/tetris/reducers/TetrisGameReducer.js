import Constants from "../util/TetrisConstants"
import { createEmptyRow, create2DArray } from "../util/TetrisUtil"

const DEFAULT_BOARD_STATE = create2DArray(Constants.GRID_WIDTH, Constants.GRID_HEIGHT, Constants.CELL_EMPTY);
const TETROMINO_OFFSET = Math.floor(Constants.TETROMINO_WIDTH / 2);

const DEFAULT_STATE = {
    gameState: Constants.GAME_STATE_IDLE,
    tetromino: null,
    nextTetromino: null,
    board: DEFAULT_BOARD_STATE
};

export default (state = DEFAULT_STATE, action) => {
    if (!action)
        return state;
    switch (action.type) {
        default:
            return DEFAULT_STATE;
        case "TETRIS_UPDATE_SCORE":
            return { ...state, score: action.score };
        case "TETRIS_RESET_GAME":
            return DEFAULT_STATE;
        case "TETRIS_START_GAME":
            return { ...state, gameState: Constants.GAME_STATE_IN_PROGRESS };
        case "TETRIS_GAME_OVER":
            return { ...state, gameState: Constants.GAME_STATE_GAME_OVER };
        case "TETRIS_MOVE":
            let position = action.position || (state.tetromino||{}).position;
            let rotation = action.rotation;
            if (rotation == null) {
                rotation = (state.tetromino || {}).rotation || Constants.ROTATION_0;
            }
            let shape = action.shape || state.tetromino.shape;
            return { ...state, tetromino: { ...state.tetromino, position, rotation, shape } };
        case "TETRIS_SET_NEXT": return (({ shape, position, rotation}) => {
            return {...state, nextTetromino: { shape, position, rotation }};
        })(action);
        case "TETRIS_POP": return (({ shape, position, rotation }) => (
            {...state, tetromino: state.nextTetromino, nextTetromino: { shape, position, rotation }}
        ))(action);
        case "TETRIS_ROTATE": return (({ rotation }) => (
            {...state, tetromino: {...state.tetromino, rotation}}
        ))(action);
        case "BOARD_RESET":
            return { ...state, board: DEFAULT_BOARD_STATE };
        case "TETRIS_FREEZE": return (() => {
            const updatedBoard = [ ...state.board ];
            const { position, shape, rotation } = state.tetromino;

            for (let v = 0; v < Constants.TETROMINO_WIDTH; v++) {
                const row = position.y + v - TETROMINO_OFFSET;
                if (row < 0 || row > Constants.GRID_HEIGHT - 1)
                    continue;

                updatedBoard[row] = [...updatedBoard[row]];

                for (let u = 0; u < Constants.TETROMINO_WIDTH; u++) {
                    const column = position.x + u - TETROMINO_OFFSET;
                    if (column < 0 || column > Constants.GRID_WIDTH - 1)
                        continue;

                    if (shape[rotation][Constants.TETROMINO_WIDTH * v + u]) {
                        updatedBoard[row][column] = Constants.CELL_FILLED;
                    }
                }
            }

            return { ...state, board: updatedBoard };
        })();
        case "TETRIS_CLEAR_ROW": return (({y}) => {
            const updatedBoard = [ ...state.board ];

            updatedBoard.splice(y, 1);
            updatedBoard.splice(0, 0, createEmptyRow(Constants.GRID_WIDTH));

            return { ...state, board: updatedBoard };
        })(action);
    }
};

