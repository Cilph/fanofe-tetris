export default (scope) => ({
    start() {
        return { type: "TETRIS_START_GAME", scope: scope }
    },

    reset() {
        return { type: "TETRIS_RESET_GAME", scope: scope }
    },

    rotate() {
        return { type: "TETRIS_ROTATE", scope: scope }
    },

    move(position, rotation) {
        return { type: "TETRIS_MOVE", scope, position, rotation }
    },

    setNext(shape, position, rotation) {
        return { type: "TETRIS_SET_NEXT", scope, shape, position, rotation }
    },

    pop(shape, position, rotation) {
        return { type: "TETRIS_POP", scope, shape, position, rotation }
    },

    rotate(rotation) {
        return { type: "TETRIS_ROTATE", scope, rotation }
    },

    clearLine(y) {
        return { type: "TETRIS_CLEAR_ROW", scope, y }
    },

    freezeTetromino() {
        return { type: "TETRIS_FREEZE", scope }
    },

    gameOver() {

    }
})