import * as React from "react"
import TetrisGame from "./TetrisGame.js"

const TetrisPage = () => <div>
    <TetrisGame scope="tetris.main"/>
</div>;

export default TetrisPage;