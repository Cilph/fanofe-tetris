import React from "react"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import TetrisBoard from "../components/TetrisBoard.js"
import TetrisGameActions from "../actions/TetrisGameActions.js"
import TetrisPanel from "../components/TetrisPanel"
import { detectCollision, generateRandomTetromino, rotateLeft, rotateRight } from "../util/TetrisUtil"
import Constants from "../util/TetrisConstants";

import "../styles/tetris.scss"

function mapStateToProps(state, props) {
    return resolvePath(state, props.scope || "") || {};
}

function mapDispatchToProps(dispatch, props) {
    return bindActionCreators(TetrisGameActions(props.scope), dispatch)
}

@connect(mapStateToProps, mapDispatchToProps)
export default class TetrisGame extends React.Component {
    constructor(props) {
        super(props);

        this.keyHandlers = {
            "ArrowUp": this.handlePressRotateLeft,
            "ArrowDown": this.handlePressRotateRight,
            "ArrowLeft": this.handlePressMoveLeft,
            "ArrowRight": this.handlePressMoveRight,
            " ": this.handlePressDrop
        }
    }

    componentDidMount() {

    }

    componentDidUpdate(prevProps) {
        const { gameState } = this.props;
        if (gameState != prevProps.gameState) {
            if (gameState == Constants.GAME_STATE_IN_PROGRESS) {
                this.startTickTimer();
            } else {
                if (this.timer != null) {
                    clearInterval(this.timer);
                }
            }
        }
    }

    componentWillUnmount() {
        if (this.timer != null) {
            clearInterval(this.timer);
        }
    }

    startTickTimer() {
        this.timer = setInterval(() => {
            this.handleTimerTick()
        }, Constants.TICK_INTERVAL)
    }

    render() {
        return <div className="tetris" tabIndex="0" onKeyDown={this.handleKeyDown}>
            <TetrisBoard board={this.props.board} tetromino={this.props.tetromino}/>
            <TetrisPanel score={this.props.score} nextTetromino={this.props.nextTetromino}/>
            <button onClick={this.handlePressStartGame}>Start</button>
            <button onClick={this.handlePressReset}>Reset</button>
        </div>
    }

    handleTimerTick=()=> {
        this.doHandleGravity();
    };

    handlePressRotateLeft=()=> {
        const { gameState, tetromino } = this.props;
        if (gameState != Constants.GAME_STATE_IN_PROGRESS)
            return;

        const newRotation = rotateLeft(tetromino.rotation);

        this.doHandleUpdateTetromino(null, newRotation);
    };

    handlePressRotateRight=()=> {
        const { gameState, tetromino } = this.props;
        if (gameState != Constants.GAME_STATE_IN_PROGRESS)
            return;

        const newRotation = rotateRight(tetromino.rotation);

        this.doHandleUpdateTetromino(null, newRotation);
    };

    handlePressMoveLeft=()=> {
        const { gameState, tetromino } = this.props;
        if (gameState != Constants.GAME_STATE_IN_PROGRESS)
            return;

        const newPosition = { x: tetromino.position.x - 1, y: tetromino.position.y };

        this.doHandleUpdateTetromino(newPosition, null);
    };

    handlePressMoveRight=()=> {
        const { gameState, tetromino } = this.props;
        if (gameState != Constants.GAME_STATE_IN_PROGRESS)
            return;

        const newPosition = { x: tetromino.position.x + 1, y: tetromino.position.y };

        this.doHandleUpdateTetromino(newPosition, null);
    };

    handlePressDrop=()=> {
        const { board, gameState, tetromino } = this.props;
        if (gameState != Constants.GAME_STATE_IN_PROGRESS)
            return;

        let candidateY = tetromino.position.y;
        const candidatePosition = { x: tetromino.position.x, y: candidateY };
        for (; candidateY < Constants.GRID_HEIGHT; candidateY++) {
            candidatePosition.y = candidateY;
            if (detectCollision(board, candidatePosition, tetromino.shape, tetromino.rotation)) {
                candidatePosition.y = candidateY - 1;
                break;
            }
        }

        if (candidateY != tetromino.position.y) {
            this.doHandleUpdateTetromino(candidatePosition, null);
        }
    };

    handlePressStartGame=()=> {
        const { gameState } = this.props;
        if (gameState == Constants.GAME_STATE_IN_PROGRESS)
            return;

        this.popTetromino();
        this.props.start();
    };

    handlePressReset=()=> {
        this.props.reset();
    };

    doHandleUpdateTetromino(position, rotation) {
        const { board, tetromino } = this.props;
        position = position || tetromino.position;
        if (rotation == null) {
            rotation = tetromino.rotation;
        }

        if (detectCollision(board, position, tetromino.shape, rotation))
            return false;

        this.props.move(position);

        if (rotation != tetromino.rotation) {
            this.props.rotate(rotation);
        }

        return true;
    }

    doHandleGravity=()=> {
        let { board, tetromino } = this.props;
        const updatedPosition = { x: tetromino.position.x, y: tetromino.position.y + 1};
        const success = this.doHandleUpdateTetromino(updatedPosition, null);
        if (!success) {
            this.props.freezeTetromino();

            const { shape, position, rotation } = this.popTetromino();

            if (detectCollision(board, position, shape, rotation)) {
                this.props.freezeTetromino();
                this.props.gameOver();
            }
        }
    };

    popTetromino=()=> {
        let tetromino = this.props.nextTetromino;
        if (tetromino == null) {
            tetromino = this.createNewTetromino();
            this.props.setNext(tetromino.shape, tetromino.position, tetromino.rotation);
        }

        const nextTetromino = this.createNewTetromino();
        this.props.pop(nextTetromino.shape, nextTetromino.position, nextTetromino.rotation);

        return tetromino;
    };

    createNewTetromino() {
        const shape = Constants.TETROMINO_SHAPES[Object.keys(Constants.TETROMINO_SHAPES)[
            Math.floor(Math.random() * Object.keys(Constants.TETROMINO_SHAPES).length)
        ]];

        // TODO: Properly spawn piece
        const position = { x: Math.floor(Constants.GRID_WIDTH / 2), y: 2 };
        const rotation = Constants.ROTATION_0;

        return { shape, position, rotation };
    };

    handleKeyDown=(e) => {
        if (this.keyHandlers[e.key]) {
            this.keyHandlers[e.key].apply(this);
        }

        e.stopPropagation();
    };
}

function resolvePath(obj, path) {
    const props = path.split(".");
    let i = 0;

    for (let prop; i < props.length - 1; i++) {
        prop = props[i];

        if (obj[prop] == null)
            return undefined;
        obj = obj[prop];

    }

    return obj[props[i]];
}