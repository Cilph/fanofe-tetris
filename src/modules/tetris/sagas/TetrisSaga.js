import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import Constants from "../util/TetrisConstants"

const STATE_SELECTOR = (scope) => s => s.tetris[scope];

function* handleTetrisLines() {
    yield takeEvery("TETRIS_FREEZE", function*(action) {
        const scope = action.scope;
        const state = yield select(STATE_SELECTOR);
        const { board } = state;

        let score = 0;
        for (let y = 0; y < board.length; y++) {
            if (isRowFilled(board[y])) {
                score += 100;
                yield put({ type: "TETRIS_CLEAR_ROW", scope, y})
            }
        }

        if (score > 0)
            yield put({ type: "GAME_UPDATE_SCORE", scope, score });
    });
}

function isRowFilled(row) {
    for (let x = 0; x < row.length; x++) {
        if (Constants.CELL_EMPTY === row[x])
            return false;
    }

    return true;
}