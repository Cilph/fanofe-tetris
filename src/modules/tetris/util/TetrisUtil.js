import Constants from "./TetrisConstants.js"

const TETROMINO_OFFSET = Math.floor(Constants.TETROMINO_WIDTH / 2);

export function newArray(length) {
    return Array.apply(null, new Array(length));
}

export function createEmptyRow(width, defaultValue) {
    return newArray(width)
        .map(x => defaultValue === undefined ? Constants.CELL_EMPTY : defaultValue);
}

export function create2DArray(width, height, defaultValue) {
    return newArray(height)
        .map(x => createEmptyRow(width, defaultValue));
}

export function overlayTetromino(row, y, tetromino) {
    if (!tetromino)
        return row;

    if (y < tetromino.position.y - TETROMINO_OFFSET || y > tetromino.position.y + TETROMINO_OFFSET)
        return row;

    const v =  y - tetromino.position.y + TETROMINO_OFFSET;

    const overlayedRow = [ ...row ];
    for (let u = 0; u < Constants.TETROMINO_WIDTH; u++) {
        if (tetromino.shape[tetromino.rotation][v * Constants.TETROMINO_WIDTH + u])
            overlayedRow[tetromino.position.x + u - TETROMINO_OFFSET] = Constants.CELL_FILLED;
    }

    return overlayedRow;
}

export function detectCollision(board, position, shape, rotation) {
    // Iterate over the TETROMINO_WIDTH x TETROMINO_WIDTH area, check for out of bounds and collision.
    for (let i = 0; i < Constants.TETROMINO_WIDTH; i++) {
        for (let j = 0; j < Constants.TETROMINO_WIDTH; j++) {
            let x = position.x + j - TETROMINO_OFFSET;
            let y = position.y + i - TETROMINO_OFFSET;
            if (x < 0 || x > Constants.GRID_WIDTH - 1 || y > Constants.GRID_HEIGHT - 1) {
                const idx = i * Constants.TETROMINO_WIDTH + j;
                if (shape[rotation][idx])
                    return true;
            } else if (y >= 0) {
                const idx = i * Constants.TETROMINO_WIDTH + j;
                const block = shape[rotation][idx];
                const matrix = board[y][x];
                if (matrix != Constants.CELL_EMPTY && block)
                    return true;
            }
        }
    }

    return false;
}

export function rotateLeft(rotation) {
    switch (rotation) {
        default:
        case Constants.ROTATION_0:
            return Constants.ROTATION_270;
        case Constants.ROTATION_90:
            return Constants.ROTATION_0;
        case Constants.ROTATION_180:
            return Constants.ROTATION_90;
        case Constants.ROTATION_270:
            return Constants.ROTATION_180;
    }
}

export function rotateRight(rotation) {
    switch (rotation) {
        default:
        case Constants.ROTATION_0:
            return Constants.ROTATION_90;
        case Constants.ROTATION_90:
            return Constants.ROTATION_180;
        case Constants.ROTATION_180:
            return Constants.ROTATION_270;
        case Constants.ROTATION_270:
            return Constants.ROTATION_0;
    }
}