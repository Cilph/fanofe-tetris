import React from 'react';
import TetrisRow from "./TetrisRow.js"
import Constants from "../util/TetrisConstants"
import TetrisPreview from "../components/TetrisPreview"

const TetrisPanel = ({ score, nextTetromino }) =>
    <div className="tetris__panel">
        <span className="tetris__header">Score:</span>
        <span>{score}</span>
        <span className="tetris__header">Next:</span>
        <TetrisPreview tetromino={nextTetromino}/>
    </div>;

export default TetrisPanel;