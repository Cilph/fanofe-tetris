import React from 'react';
import TetrisRow from "./TetrisRow.js"
import Constants from "../util/TetrisConstants"
import { overlayTetromino } from "../util/TetrisUtil"

const TETROMINO_OFFSET = Math.floor(Constants.TETROMINO_WIDTH / 2);

const TetrisBoard = ({ board, tetromino }) =>
    <div className="tetris__board">
        {board.map((r, idx) =>
            <TetrisRow key={idx} row={overlayTetromino(r, idx, tetromino)}/>
        )}
    </div>;

export default TetrisBoard;