import * as React from "react"

import TetrisBoard from "../components/TetrisBoard"
import Constants from "../util/TetrisConstants.js"
import { newArray, create2DArray } from "../util/TetrisUtil.js"

const TetrisPreview = ({ tetromino }) => {
    const board = create2DArray(Constants.TETROMINO_WIDTH, Constants.TETROMINO_WIDTH, Constants.CELL_EMPTY);
    const previewTetromino = tetromino == null ? null : {
        position: { x: 2, y: 2 },
        shape: tetromino.shape,
        rotation: Constants.ROTATION_0
    };

   return <div className="tetris__preview">
       <TetrisBoard board={board} tetromino={previewTetromino}/>
   </div>;
};

export default TetrisPreview;