import React from 'react';
import TetrisCell from "./TetrisCell.js"

const TetrisRow = ({ row }) =>
    <div className="tetris__row">
        {row.map((c, idx) =>
            <TetrisCell key={idx} cell={c}/>
        )}
    </div>;


export default TetrisRow;