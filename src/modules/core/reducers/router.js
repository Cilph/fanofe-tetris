import { LOCATION_CHANGE } from 'react-router-redux';

const initialState = ({
	locationBeforeTransition: undefined,
});

export default function(state = initialState, { type, payload }) {
	if (type === LOCATION_CHANGE) {
		return { ...state,  locationBeforeTransitions: payload };
	}
	return state;
};
