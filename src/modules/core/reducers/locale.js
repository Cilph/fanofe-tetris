export default function localeReducer(state = 'en', action) {
	if (action.type === 'LOCALE_UPDATE') return action.payload;
	return state;
}
