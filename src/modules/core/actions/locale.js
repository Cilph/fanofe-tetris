export function setLocale(locale = 'en') {
	return { type: 'LOCALE_UPDATE', payload: locale };
};
