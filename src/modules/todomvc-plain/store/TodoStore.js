let counter = 0;

export default class TodoStore {
    _todos = [];
    _subscribers = [];

    constructor() {

    }

    allTodo() {
        return this._todos;
    }

    toggleAllTodo(toggled) {
        this._todos.forEach((t, idx) => {
            this._todos[idx] = { ...t, toggled }
        });

        this.notifySubscribers();
    }

    addTodo(text, toggled=false) {
        this._todos.push({
            id: counter++,
            text,
            toggled
        });

        this.notifySubscribers();
    }

    saveTodo(id, text, toggled) {
        const arrayIdx = this._todos.findIndex(i => i.id === id);
        if (arrayIdx < 0)
            return;

        this._todos[arrayIdx] = {
            id,
            text: text === undefined ? this._todos[arrayIdx].text : text,
            toggled: toggled === undefined ? this._todos[arrayIdx].toggled : toggled
        };

        this.notifySubscribers();
    }

    deleteTodo(id) {
        this._todos = this._todos.filter(t => t.id !== id);

        this.notifySubscribers();
    }

    subscribe(callback) {
        this._subscribers.push(callback);
    }

    unsubscribe(callback) {
        this._subscribers = this._subscribers.predicate(s => s === callback);
    }

    notifySubscribers() {
        for (let sub of this._subscribers) {
            try { sub(); } catch (ex) { console.log(ex); }
        }
    }
}