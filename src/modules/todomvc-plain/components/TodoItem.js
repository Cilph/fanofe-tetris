import * as React from "react";

export default class TodoItem extends React.Component {
    editingTextInput;

    static propTypes = {
        id: React.PropTypes.any.isRequired,
        text: React.PropTypes.string.isRequired,
        toggled: React.PropTypes.bool.isRequired,
        editing: React.PropTypes.bool.isRequired,

        onToggle: React.PropTypes.func,
        onDelete: React.PropTypes.func,
        onStartEdit: React.PropTypes.func,
        onConfirm: React.PropTypes.func,
        onCancel: React.PropTypes.func,
    };

    constructor(props) {
        super(props);

        this.state = {
            editingText: ""
        }
    }

    render() {
        const { text, toggled, editing } = this.props;

        return <div className="todo__item">
            <input type="checkbox" className="todo__toggle" value={toggled} checked={toggled}
                   onClick={this.handleToggle}/>
            <label style={{ display: editing ? "none" : "" }}
                   onDoubleClick={this.handleStartEdit}
            >{text}</label>
            <input type="text" className="todo__text"
                   ref={i => this.editingTextInput = i}
                   style={{ display: editing ? "" : "none" }}
                   disabled={!editing}
                   onBlur={this.handleSubmit}
                   onChange={this.handleChange}
                   onKeyDown={this.handleKeyDown}
                   value={this.state.editingText}/>
            <button type="button" className="todo__button todo__button--remove"
                    onClick={this.handleDelete}
            >X</button>
        </div>
    }

    handleKeyDown=(e)=> {
        console.log(e.key);

        switch(e.key) {
            case "Enter":
                this.handleSubmit();
                break;
            case "Escape":
                this.handleCancel();
                break;
        }
    };

    handleChange=(e)=> {
        this.setState({
            ...this.state,
            editingText: e.target.value
        });
    };

    handleStartEdit=()=> {
        if (this.props.onStartEdit) {
            this.props.onStartEdit(this.props.id);
        }

        this.editingTextInput.focus();

        this.setState({
            ...this.state,
            editingText: this.props.text
        })
    };

    handleDelete=()=> {
        if (this.props.onDelete) {
            this.props.onDelete(this.props.id);
        }
    };

    handleToggle=(e)=> {
        if (this.props.onToggle) {
            this.props.onToggle(this.props.id, e.target.checked);
        }
    };

    handleCancel=()=> {
        if (this.props.onCancel) {
            this.props.onCancel(this.props.id);
        }
    };

    handleSubmit=()=> {
        const submittedText = this.state.editingText;

        this.setState({
            ...this.state
        });

        if (this.props.onConfirm) {
            this.props.onConfirm(this.props.id, submittedText);
        }
    }
};


