import * as React from "react";

const TodoFilterButton = ({value, onClick, selected}) => {
    return <button className={selected ? "todo__filter todo__filter--selected" : "todo__filter"}
                   onClick={onClick}
    >{value}</button>
};

export default TodoFilterButton;