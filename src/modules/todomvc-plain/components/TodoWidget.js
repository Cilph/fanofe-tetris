import * as React from "react";
import TodoHeader from "./TodoHeader";
import TodoItem from "./TodoItem";
import TodoFilterButton from "./TodoFilter";

import "../styles/todomvc.scss";

const FILTER_ALL = { name: "Alles", predicate: t => true };
const FILTER_ACTIVE = { name: "Open", predicate: t => !t.toggled };
const FILTER_COMPLETED = { name: "Afgerond", predicate: t => t.toggled };
const FILTERS = [ FILTER_ALL, FILTER_ACTIVE, FILTER_COMPLETED ];

export default class TodoWidget extends React.Component {
    static propTypes = {
        store: React.PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            filter: FILTER_ALL,
            active: -1,
            newTodo: "",
            editingTodo: -1,
        }
    }

    render() {
        const allTodo = this.props.store.allTodo();
        const filteredTodos = allTodo.filter(this.state.filter.predicate);

        return <section className="todo">
            <TodoHeader/>
            <section className="todo__frame">
                <header className="todo__new">
                    <input className="todo__new-input" type="text" value={this.state.newTodo} onKeyDown={this.handleNewTodoKeyDown} onChange={this.handleNewTodoChange}/>
                </header>
                <section className="todo__todos">
                    <ul className="todo__todo-items">
                        {filteredTodos.map(t => <li key={t.id}>
                            <TodoItem
                                      id={t.id}
                                      text={t.text}
                                      toggled={t.toggled}
                                      editing={t.id === this.state.editingTodo}
                                      onToggle={this.handleTodoToggle}
                                      onDelete={this.handleTodoDelete}
                                      onStartEdit={this.handleTodoStartEdit}
                                      onConfirm={this.handleTodoSubmit}
                                      onCancel={this.handleTodoCancel}
                            /></li>)}
                    </ul>
                </section>
                <footer className="todo__footer">
                    <ul className="todo__footer-group">
                        <li>{allTodo.length} taken te verwerken.</li>
                        <li>
                            <ul className="todo__footer-button-group">
                            { FILTERS.map((f, idx) => <li key={idx}>
                                    <TodoFilterButton onClick={() => this.handleFilterClick(f)} value={f.name}/>
                                </li>
                            )}
                            </ul>
                        </li>
                    </ul>
                </footer>
            </section>
        </section>
    }

    handleNewTodoKeyDown=(e)=> {
        switch (e.key) {
            case "Enter":
                this.handleNewTodoSubmit();
                break;
        }
    };

    handleFilterClick=(filter) => {
        this.setState({
            ...this.state,
            filter
        })
    };

    handleTodoToggleAll=(e)=> {
        this.props.store.toggleAllTodo(e.target.checked)
    };

    handleTodoStartEdit=(id)=> {
        console.log(id);
        this.setState({
            ...this.state,
            editingTodo: id
        })
    };

    handleTodoToggle=(id, toggled)=> {
        this.props.store.saveTodo(id, undefined, toggled);
    };

    handleTodoCancel=(id) => {
        this.setState({
            editingTodo: -1
        });
    };

    handleTodoSubmit=(id, text) => {
        this.props.store.saveTodo(id, text);

        this.setState({
            ...this.state,
            editingTodo: -1
        });
    };

    handleTodoDelete=(id)=> {
        this.props.store.deleteTodo(id);
    };

    handleNewTodoChange=(e)=> {
        this.setState({
            ...this.state,
            newTodo: e.target.value
        })
    };

    handleNewTodoSubmit=()=> {
        this.props.store.addTodo(this.state.newTodo);

        this.setState({
            ...this.state,
            newTodo: ""
        });
    };
}