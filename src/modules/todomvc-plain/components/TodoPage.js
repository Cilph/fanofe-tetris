import * as React from "react";
import TodoStore from "../store/TodoStore.js";
import TodoWidget from "./TodoWidget.js";

/* I am knowingly not making the state immutable, kthxbye */
export default class TodoPage extends React.Component {
    store;

    constructor(props) {
        super(props);
        this.onStoreChange = this.onStoreChange.bind(this);
    }

    componentWillMount() {
        this.store = new TodoStore();
        this.store.subscribe(this.onStoreChange)
    }

    componentWillUnmount() {
        this.store.unsubscribe()
    }

    onStoreChange() {
        this.forceUpdate();
    }

    render() {
        return <div>
            <TodoWidget store={this.store}/>
        </div>;
    }
}